package com.atlassianlab.bamboo.plugins.afterdeploymenttrigger;


import com.atlassian.bamboo.build.BuildDetectionAction;
import com.atlassian.bamboo.build.BuildDetectionActionFactory;
import com.atlassian.bamboo.builder.BuildState;
import com.atlassian.bamboo.deployments.environments.Environment;
import com.atlassian.bamboo.deployments.execution.events.DeploymentFinishedEvent;
import com.atlassian.bamboo.deployments.projects.service.DeploymentProjectService;
import com.atlassian.bamboo.deployments.results.DeploymentResult;
import com.atlassian.bamboo.deployments.results.service.DeploymentResultService;
import com.atlassian.bamboo.plan.NonBlockingPlanExecutionService;
import com.atlassian.bamboo.plan.cache.CachedPlanManager;
import com.atlassian.bamboo.plan.cache.ImmutableTopLevelPlan;
import com.atlassian.bamboo.plan.trigger.TriggerManager;
import com.atlassian.bamboo.trigger.TriggerDefinition;
import com.atlassian.bamboo.trigger.TriggerDefinitionImpl;
import com.atlassian.bamboo.trigger.Triggerable;
import com.atlassian.bamboo.v2.build.trigger.TriggerReason;
import com.google.common.collect.ImmutableMap;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.jetbrains.annotations.NotNull;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.verification.VerificationMode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.atlassian.bamboo.builder.BuildState.FAILED;
import static com.atlassian.bamboo.builder.BuildState.SUCCESS;
import static com.atlassianlab.bamboo.plugins.afterdeploymenttrigger.AfterDeploymentTriggerConfigurator.ANY_ENVIRONMENT_ID;
import static com.atlassianlab.bamboo.plugins.afterdeploymenttrigger.DeploymentResultListener.PLUGIN_KEY;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(JUnitParamsRunner.class)
public class DeploymentResultListenerTest
{
    @Rule public MockitoRule mockitoRule = MockitoJUnit.rule();
    @Mock private DeploymentResultService deploymentResultService;
    @Mock private CachedPlanManager planManager;
    @Mock private BuildDetectionActionFactory buildDetectionActionFactory;
    @Mock private NonBlockingPlanExecutionService nonBlockingPlanExecutionService;
    @Mock private TriggerManager triggerManager;
    @Mock private DeploymentProjectService deploymentProjectService;

    @InjectMocks private DeploymentResultListener listener;

    @Before
    public void setUp()
    {
        when(buildDetectionActionFactory.createBuildDetectionActionForPluginBuildTrigger(any(), isNull(), isA(TriggerReason.class), isNull(), anyMap(), anyMap())).thenReturn(mock(BuildDetectionAction.class));
        when(triggerManager.getTriggerReason(ArgumentMatchers.eq(DeploymentResultDependencyTriggerReason.PLUGIN_KEY), anyMap())).thenReturn(mock(TriggerReason.class));
    }

    @Test
    public void doesntTriggerPlanWithoutTrigger()
    {
        listener.deploymentFinishedEventHandler(createDeploymentFinishedEvent(1, 2, SUCCESS));

        verify(nonBlockingPlanExecutionService, never()).tryToStart(any(Triggerable.class), any(BuildDetectionAction.class));
    }

    //plan has 2 triggers for different deployment environments and should be triggered for both of events
    @Test
    public void triggersPlanWithTwoMatchingConfigurations()
    {
        final ImmutableTopLevelPlan plan = mock(ImmutableTopLevelPlan.class);
        final List<TriggerDefinition> triggerDefinitions = new ArrayList<>();
        triggerDefinitions.add(createTriggerDefinition(1, 1));
        triggerDefinitions.add(createTriggerDefinition(2, 2));
        when(plan.getTriggerDefinitions()).thenReturn(triggerDefinitions);
        when(planManager.getPlans()).thenReturn(Collections.singletonList(plan));
        listener.deploymentFinishedEventHandler(createDeploymentFinishedEvent(1, 1, SUCCESS));
        listener.deploymentFinishedEventHandler(createDeploymentFinishedEvent(2, 2, SUCCESS));

        verify(nonBlockingPlanExecutionService, times(2)).tryToStart(eq(plan), any(BuildDetectionAction.class));
    }

    @Test
    public void exceptionOnPlanExecutionShouldNotPreventOtherPlansFromTriggering()
    {
        final DeploymentFinishedEvent event = createDeploymentFinishedEvent(1, 1, SUCCESS);
        final ImmutableTopLevelPlan plan1 = getTriggerablePlan(1, 1);
        final ImmutableTopLevelPlan plan2 = getTriggerablePlan(1, 1);
        List<ImmutableTopLevelPlan> plans = new ArrayList<>();
        plans.add(plan1);
        plans.add(plan2);
        when(planManager.getPlans()).thenReturn((plans));
        when(nonBlockingPlanExecutionService.tryToStart(eq(plan1), any(BuildDetectionAction.class))).thenThrow(new RuntimeException("test"));

        listener.deploymentFinishedEventHandler(event);

        verify(nonBlockingPlanExecutionService).tryToStart(eq(plan1), any(BuildDetectionAction.class));
        verify(nonBlockingPlanExecutionService).tryToStart(eq(plan2), any(BuildDetectionAction.class));
    }

    @Test
    @Parameters(method = "parametersForTriggersPlanWithMatchingConfiguration")
    public void triggersPlanWithMatchingConfiguration(long eventDeploymentProjectId,
                                                      long eventDeploymentEnvironmentId,
                                                      final BuildState eventDeploymentResultStatus,
                                                      long triggerDeploymentProjectId,
                                                      long triggerDeploymentEnvironmentId,
                                                      final VerificationMode planTriggered)
    {
        final ImmutableTopLevelPlan plan = getTriggerablePlan(triggerDeploymentProjectId,
                triggerDeploymentEnvironmentId);
        when(planManager.getPlans()).thenReturn(Collections.singletonList(plan));

        final DeploymentFinishedEvent event = createDeploymentFinishedEvent(eventDeploymentProjectId,
                eventDeploymentEnvironmentId,
                eventDeploymentResultStatus);
        listener.deploymentFinishedEventHandler(event);

        verify(nonBlockingPlanExecutionService, planTriggered).tryToStart(eq(plan), any(BuildDetectionAction.class));
    }

    @SuppressWarnings("unused")
    private Object[] parametersForTriggersPlanWithMatchingConfiguration()
    {
        return new Object[][]{
                {1, 1, SUCCESS, 1, ANY_ENVIRONMENT_ID, atLeastOnce()}, //matching deployment project
                {1, 1, SUCCESS, 1, 1, atLeastOnce()}, //matching environment
                {1, 1, SUCCESS, 2, ANY_ENVIRONMENT_ID, never()}, //not matching deployment project
                {1, 1, SUCCESS, 1, 2, never()}, //not matching environment
                {1, 1, FAILED, 1, ANY_ENVIRONMENT_ID, never()}, //not matching deployment result state
                {1, 1, FAILED, 1, 1, never()} //not matching deployment result state
        };
    }

    @NotNull
    private DeploymentFinishedEvent createDeploymentFinishedEvent(long deploymentProjectId,
                                                                  long environmentId,
                                                                  BuildState buildState)
    {
        final long deploymentResultId = 1L;
        final DeploymentResult deploymentResult = createDeploymentResult(deploymentResultId);
        final Environment environment = createEnvironment(deploymentProjectId, environmentId);
        when(deploymentResult.getEnvironment()).thenReturn(environment);
        when(deploymentResult.getDeploymentState()).thenReturn(buildState);
        return new DeploymentFinishedEvent(deploymentResultId);
    }

    @NotNull
    private DeploymentResult createDeploymentResult(long deploymentResultId)
    {
        final DeploymentResult deploymentResult = mock(DeploymentResult.class);
        when(deploymentResultService.getDeploymentResult(deploymentResultId)).thenReturn(deploymentResult);
        return deploymentResult;
    }

    @NotNull
    private Environment createEnvironment(long deploymentProjectId, long environmentId)
    {
        final Environment environment = mock(Environment.class);
        when(environment.getDeploymentProjectId()).thenReturn(deploymentProjectId);
        when(environment.getId()).thenReturn(environmentId);
        return environment;
    }

    private ImmutableTopLevelPlan getTriggerablePlan(long deploymentProjectId,
                                                     long environmentId)
    {
        final ImmutableTopLevelPlan plan = mock(ImmutableTopLevelPlan.class);
        when(plan.getTriggerDefinitions()).thenReturn(Collections.singletonList(createTriggerDefinition(deploymentProjectId, environmentId)));
        return plan;
    }

    private TriggerDefinition createTriggerDefinition(long deploymentProjectId, long environmentId)
    {
        final ImmutableMap<String, String> configuration = ImmutableMap.of(AfterDeploymentTriggerConfigurator.CFG_DEPLOYMENT_ENVIRONMENT_ID, String.valueOf(environmentId),
                AfterDeploymentTriggerConfigurator.CFG_DEPLOYMENT_PROJECT_ID, String.valueOf(deploymentProjectId));
        return new TriggerDefinitionImpl.Builder()
                .id(1L)
                .pluginKey(PLUGIN_KEY)
                .name("After deployment")
                .configuration(configuration)
                .build();
    }
}