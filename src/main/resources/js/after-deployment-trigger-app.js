define('feature/trigger/after-deployment-trigger-app', [
    'jquery',
    'marionette',
    'aui/flag',
    'util/ajax',
    'underscore',
    'aui'
], function($,
            Marionette,
            Flag,
            ajax,
            _,
            AJS) {
    'use strict';

    return Marionette.Application.extend({
        $deploymentProject: null,
        $deploymentEnvironment: null,
        settings: {
            deploymentProject: null,
            deploymentEnvironment: null,
            ajax: {
                baseUrl: AJS.contextPath() + '/rest/afterdeploymenttrigger/latest/afterDeployment/',
                dataType: 'json'
            }
        },

        initialize: function(options) {
            var settings = options || {};
            this.settings.deploymentProject = settings.deploymentProject;
            this.settings.deploymentEnvironment = settings.deploymentEnvironment;
            this.$deploymentProject = $(this.settings.deploymentProject).select2({enable: false, width: 'copy'});
            this.$deploymentEnvironment = $(this.settings.deploymentEnvironment).select2({
                enable: false,
                width: 'copy',
                query: function() {
                }
            });
        },

        onStart: function() {
            $(this.$deploymentProject).on('change', _.bind(function(event) {
                this.$deploymentEnvironment.val('-1');
                var selectedDeploymentProjectId = $(event.target).val();
                this.fetchDeploymentEnvironments(selectedDeploymentProjectId);
            }, this));
            var selectedDeploymentProjectId = this.$deploymentProject.val();
            if (selectedDeploymentProjectId !== 0 &&
                selectedDeploymentProjectId !== '') {
                this.fetchDeploymentEnvironments(selectedDeploymentProjectId);
            }
        },
        fetchDeploymentEnvironments: function(selectedDeploymentProjectId) {
            this.$deploymentEnvironment.select2({data: []});
            if (selectedDeploymentProjectId === '0') {
                return;
            }

            var settings = {
                url: this.settings.ajax.baseUrl + selectedDeploymentProjectId + '/environments',
                error: this.onFetchErrorHandler
            };
            ajax(settings)
                .done(_.bind(function(data) {
                    var options = $.map(data, function(env) {
                        env.text = env.name;
                        return env;
                    });
                    options.splice(0, 0, {
                        id: 0,
                        text: AJS.I18n.getText('deployment.trigger.afterDeployment.environment.any')
                    });
                    this.$deploymentEnvironment.select2({data: options});
                }, this));
        },
        onFetchErrorHandler: function(collection, response, options) {
            new Flag({
                type: 'error',
                close: 'manual',
                body: AJS.I18n.getText('deployment.trigger.afterDeployment.fetch.error')
            });
        }
    });
});
