package com.atlassianlab.bamboo.plugins.afterdeploymenttrigger;

import com.atlassian.bamboo.build.BuildDetectionAction;
import com.atlassian.bamboo.build.BuildDetectionActionFactory;
import com.atlassian.bamboo.builder.BuildState;
import com.atlassian.bamboo.deployments.environments.Environment;
import com.atlassian.bamboo.deployments.execution.events.DeploymentFinishedEvent;
import com.atlassian.bamboo.deployments.projects.DeploymentProject;
import com.atlassian.bamboo.deployments.projects.service.DeploymentProjectService;
import com.atlassian.bamboo.deployments.results.DeploymentResult;
import com.atlassian.bamboo.deployments.results.service.DeploymentResultService;
import com.atlassian.bamboo.deployments.versions.DeploymentVersion;
import com.atlassian.bamboo.plan.NonBlockingPlanExecutionService;
import com.atlassian.bamboo.plan.cache.CachedPlanManager;
import com.atlassian.bamboo.plan.cache.ImmutableTopLevelPlan;
import com.atlassian.bamboo.plan.trigger.PlanTrigger;
import com.atlassian.bamboo.plan.trigger.TriggerManager;
import com.atlassian.bamboo.trigger.TriggerDefinition;
import com.atlassian.bamboo.v2.build.trigger.TriggerReason;
import com.atlassian.event.api.EventListener;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.atlassianlab.bamboo.plugins.afterdeploymenttrigger.AfterDeploymentTriggerConfigurator.CFG_DEPLOYMENT_ENVIRONMENT_ID;
import static com.atlassianlab.bamboo.plugins.afterdeploymenttrigger.AfterDeploymentTriggerConfigurator.CFG_DEPLOYMENT_PROJECT_ID;
import static com.atlassianlab.bamboo.plugins.afterdeploymenttrigger.DeploymentResultPlanTrigger.TRIGGER_KEY;
import static java.lang.Long.valueOf;

public class DeploymentResultListener
{
    static final String PLUGIN_KEY = "com.atlassianlab.bamboo.plugins.bamboo-after-deployment-trigger-plugin:afterDeployment";
    private static final Logger log = Logger.getLogger(DeploymentResultListener.class);

    private final DeploymentResultService deploymentResultService;
    private final DeploymentProjectService deploymentProjectService;
    private final CachedPlanManager planManager;
    private final BuildDetectionActionFactory buildDetectionActionFactory;
    private final NonBlockingPlanExecutionService nonBlockingPlanExecutionService;
    private final TriggerManager triggerManager;

    @Autowired
    public DeploymentResultListener(@NotNull final DeploymentResultService deploymentResultService,
                                    @NotNull final DeploymentProjectService deploymentProjectService,
                                    @NotNull final CachedPlanManager planManager,
                                    @NotNull final BuildDetectionActionFactory buildDetectionActionFactory,
                                    @NotNull final NonBlockingPlanExecutionService nonBlockingPlanExecutionService,
                                    @NotNull final TriggerManager triggerManager)
    {
        this.deploymentResultService = deploymentResultService;
        this.deploymentProjectService = deploymentProjectService;
        this.planManager = planManager;
        this.buildDetectionActionFactory = buildDetectionActionFactory;
        this.nonBlockingPlanExecutionService = nonBlockingPlanExecutionService;
        this.triggerManager = triggerManager;
    }

    @EventListener
    public void deploymentFinishedEventHandler(@NotNull DeploymentFinishedEvent event)
    {
        final long deploymentResultId = event.getDeploymentResultId();
        final DeploymentResult deploymentResult = deploymentResultService.getDeploymentResult(deploymentResultId);
        if (deploymentResult != null)
        {
            final Environment environment = deploymentResult.getEnvironment();
            final long deploymentProjectId = environment.getDeploymentProjectId();
            final BuildState deploymentState = deploymentResult.getDeploymentState();
            final DeploymentVersion deploymentVersion = deploymentResult.getDeploymentVersion();
            final Collection<ImmutableTopLevelPlan> plansToBeTriggered = findTriggerables(deploymentProjectId,
                    environment.getId(),
                    deploymentState);
            log.debug("Found " + plansToBeTriggered.size() + " plans to be triggered");
            if (plansToBeTriggered.isEmpty())
            {
                return;
            }
            final TriggerReason triggerReason = getTriggerReason(deploymentResult, environment, deploymentVersion);
            final PlanTrigger planTrigger = triggerManager.getPlanTrigger(TRIGGER_KEY);
            for (ImmutableTopLevelPlan plan : plansToBeTriggered)
            {
                log.debug("Triggering " + plan.getKey());
                try
                {
                    final BuildDetectionAction triggeringAction = buildDetectionActionFactory.createBuildDetectionActionForPluginBuildTrigger(
                            plan,
                            null,
                            triggerReason,
                            planTrigger,
                            new HashMap<>(),
                            new HashMap<>());

                    nonBlockingPlanExecutionService.tryToStart(plan, triggeringAction);
                }
                catch (final Exception e)
                {
                    log.error(e.getMessage(), e);
                }
            }
        }
    }

    @NotNull
    private TriggerReason getTriggerReason(@NotNull final DeploymentResult deploymentResult,
                                           @NotNull final Environment environment,
                                           @Nullable final DeploymentVersion deploymentVersion)
    {
        final Map<String, String> fieldsMap = new HashMap<>();
        fieldsMap.put(DeploymentResultDependencyTriggerReason.DEPLOYMENT_PROJECT_ID_KEY, String.valueOf(environment.getDeploymentProjectId()));
        DeploymentProject deploymentProject = deploymentProjectService.getDeploymentProject(environment.getDeploymentProjectId());
        if(deploymentProject != null)
        {
            fieldsMap.put(DeploymentResultDependencyTriggerReason.DEPLOYMENT_PROJECT_NAME_KEY, deploymentProject.getName());
        }
        fieldsMap.put(DeploymentResultDependencyTriggerReason.ENVIRONMENT_ID_KEY, String.valueOf(environment.getId()));
        fieldsMap.put(DeploymentResultDependencyTriggerReason.ENVIRONMENT_NAME_KEY, environment.getName());
        fieldsMap.put(DeploymentResultDependencyTriggerReason.RESULT_ID_KEY, String.valueOf(deploymentResult.getId()));
        if (deploymentVersion != null)
        {
            fieldsMap.put(DeploymentResultDependencyTriggerReason.RELEASE_ID_KEY, String.valueOf(deploymentVersion.getId()));
            fieldsMap.put(DeploymentResultDependencyTriggerReason.RELEASE_NAME_KEY, deploymentVersion.getName());
        }
        return triggerManager.getTriggerReason(DeploymentResultDependencyTriggerReason.PLUGIN_KEY, fieldsMap);
    }

    /**
     * Search for plans which should be triggered by failure of failedPlanKey
     */
    @NotNull
    private Collection<ImmutableTopLevelPlan> findTriggerables(final long finishedDeploymentProjectId,
                                                               final long finishedDeploymentProjectEnvironmentId,
                                                               @NotNull final BuildState finishedDeploymentState)
    {
        log.debug("Looking for plans to be triggered");
        return planManager.getPlans().stream()
                .filter(input ->
                {
                    final List<TriggerDefinition> triggerDefinitions = input.getTriggerDefinitions();
                    for (TriggerDefinition triggerDefinition : triggerDefinitions)
                    {
                        final Map<String, String> configuration = triggerDefinition.getConfiguration();
                        if (triggerDefinition.isEnabled()
                                && triggerDefinition.getPluginKey().equals(PLUGIN_KEY)
                                && configuration.containsKey(CFG_DEPLOYMENT_PROJECT_ID))
                        {
                            final boolean shouldBeTriggered = isShouldBeTriggered(finishedDeploymentProjectId,
                                    finishedDeploymentProjectEnvironmentId,
                                    finishedDeploymentState,
                                    configuration);
                            if (shouldBeTriggered)
                            {
                                return true;
                            }
                        }
                    }
                    return false;
                }).collect(Collectors.toList());
    }

    private boolean isShouldBeTriggered(long finishedDeploymentProjectId,
                                        long finishedDeploymentProjectEnvironmentId,
                                        @NotNull BuildState finishedDeploymentState,
                                        final Map<String, String> configuration)
    {
        final long requiredDeploymentProjectId = valueOf(configuration.getOrDefault(CFG_DEPLOYMENT_PROJECT_ID, "0"));
        final long requiredDeploymentEnvironmentId = valueOf(configuration.getOrDefault(CFG_DEPLOYMENT_ENVIRONMENT_ID, String.valueOf(AfterDeploymentTriggerConfigurator.ANY_ENVIRONMENT_ID)));

        return finishedDeploymentProjectId == requiredDeploymentProjectId
                && isEnvironmentMatch(finishedDeploymentProjectEnvironmentId, requiredDeploymentEnvironmentId)
                && finishedDeploymentState == BuildState.SUCCESS;
    }

    private boolean isEnvironmentMatch(long finishedDeploymentProjectEnvironmentId, long requiredDeploymentEnvironmentId)
    {
        return requiredDeploymentEnvironmentId == AfterDeploymentTriggerConfigurator.ANY_ENVIRONMENT_ID
                || requiredDeploymentEnvironmentId == finishedDeploymentProjectEnvironmentId;
    }
}
