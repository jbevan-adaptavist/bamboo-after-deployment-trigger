package com.atlassianlab.bamboo.plugins.afterdeploymenttrigger;

import com.atlassian.bamboo.deployments.environments.Environment;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import static javax.xml.bind.annotation.XmlAccessType.FIELD;

@XmlRootElement(name = "environment")
@XmlAccessorType(FIELD)
public class RestEnvironment
{
    @XmlElement
    private long id;
    @XmlElement
    private String name;

    public RestEnvironment()
    {
    }

    RestEnvironment(Environment environment)
    {
        this.id = environment.getId();
        this.name = environment.getName();
    }

    public long getId()
    {
        return id;
    }

    public String getName()
    {
        return name;
    }
}
