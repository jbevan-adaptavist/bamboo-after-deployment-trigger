package com.atlassianlab.bamboo.plugins.afterdeploymenttrigger;

import com.atlassian.bamboo.plan.PlanResultKey;
import com.atlassian.bamboo.plan.cache.CachedPlanManager;
import com.atlassian.bamboo.plan.cache.ImmutableChain;
import com.atlassian.bamboo.plan.trigger.PlanTrigger;
import com.atlassian.bamboo.plan.trigger.PlanTriggerResult;
import com.atlassian.bamboo.plan.trigger.PlanTriggerResultBuilder;
import com.atlassian.bamboo.plan.vcsRevision.PlanVcsRevisionDataSet;
import com.atlassian.bamboo.repository.RepositoryException;
import com.atlassian.bamboo.util.Narrow;
import com.atlassian.bamboo.v2.build.BuildChanges;
import com.atlassian.bamboo.v2.build.trigger.TriggerReason;
import com.atlassian.bamboo.v2.trigger.ChangeDetectionManager;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.HashMap;
import java.util.Map;

public class DeploymentResultPlanTrigger implements PlanTrigger
{
    static final String TRIGGER_KEY = "com.atlassianlab.bamboo.plugins.bamboo-after-deployment-trigger-plugin:afterDeploymentPlanTrigger";
    private static final Logger log = Logger.getLogger(DeploymentResultPlanTrigger.class);

    private static final String DEPLOYMENT_PROJECT_ID = "deployment.id";
    private static final String DEPLOYMENT_PROJECT_NAME = "deployment.name";
    private static final String DEPLOYMENT_ENVIRONMENT_ID = "deployment.environmentId";
    private static final String DEPLOYMENT_ENVIRONMENT_NAME = "deployment.environmentName";
    private static final String DEPLOYMENT_RESULT_ID = "deployment.resultId";
    private static final String DEPLOYMENT_RELEASE_ID = "deployment.releaseId";
    private static final String DEPLOYMENT_RELEASE_NAME = "deployment.releaseName";

    private CachedPlanManager cachedPlanManager;
    private ChangeDetectionManager changeDetectionManager;

    @NotNull
    @Override
    public PlanTriggerResult triggerPlan(@NotNull TriggerReason triggerReason,
                                         @NotNull PlanResultKey planResultKey,
                                         @NotNull Map<String, String> params,
                                         @NotNull Map<String, String> customVariables,
                                         @Nullable PlanVcsRevisionDataSet lastVcsRevisionKeys)
    {
        final PlanTriggerResultBuilder builder = PlanTriggerResultBuilder.create(customVariables);
        final ImmutableChain chain = cachedPlanManager.getPlanByKeyIfOfType(planResultKey.getPlanKey(), ImmutableChain.class);
        if (chain == null)
        {
            throw new IllegalStateException("Could not trigger a build with reason '" + triggerReason.getName() + "' because the plan '" + planResultKey.getPlanKey() + "' does not exist");
        }

        try
        {
            final BuildChanges buildChanges = changeDetectionManager.collectAllChangesSinceLastBuild(chain, customVariables, null);
            builder.addBuildChanges(buildChanges);
        }
        catch (RepositoryException e)
        {
            final String errorMessage = "Could not detect changes for plan '" + planResultKey + "' with trigger '" + triggerReason.getName() + "'";
            log.error(errorMessage, e);
            builder.addErrorMessage(errorMessage, e);
        }

        final DeploymentResultDependencyTriggerReason deploymentTriggerReason = Narrow.to(triggerReason, DeploymentResultDependencyTriggerReason.class);
        if (deploymentTriggerReason != null)
        {
            builder.addVariable(DEPLOYMENT_PROJECT_ID, deploymentTriggerReason.getDeploymentProjectIdString());
            builder.addVariable(DEPLOYMENT_PROJECT_NAME, deploymentTriggerReason.getDeploymentProjectName());
            builder.addVariable(DEPLOYMENT_ENVIRONMENT_ID, deploymentTriggerReason.getEnvironmentIdString());
            builder.addVariable(DEPLOYMENT_ENVIRONMENT_NAME, deploymentTriggerReason.getEnvironmentName());
            builder.addVariable(DEPLOYMENT_RESULT_ID, deploymentTriggerReason.getResultIdString());
            builder.addVariable(DEPLOYMENT_RELEASE_ID, deploymentTriggerReason.getReleaseIdString());
            builder.addVariable(DEPLOYMENT_RELEASE_NAME, deploymentTriggerReason.getReleaseName());
        }

        return builder.build();
    }

    @NotNull
    @Override
    public Map<String, String> getVariablesForContinuedBuild(@NotNull TriggerReason triggerReason)
    {
        Map<String, String> result = new HashMap<>();
        final DeploymentResultDependencyTriggerReason deploymentTriggerReason = Narrow.to(triggerReason, DeploymentResultDependencyTriggerReason.class);
        if (deploymentTriggerReason != null)
        {
            result.put(DEPLOYMENT_PROJECT_ID, deploymentTriggerReason.getDeploymentProjectIdString());
            result.put(DEPLOYMENT_PROJECT_NAME, deploymentTriggerReason.getDeploymentProjectName());
            result.put(DEPLOYMENT_ENVIRONMENT_ID, deploymentTriggerReason.getEnvironmentIdString());
            result.put(DEPLOYMENT_ENVIRONMENT_NAME, deploymentTriggerReason.getEnvironmentName());
            result.put(DEPLOYMENT_RESULT_ID, deploymentTriggerReason.getResultIdString());
            result.put(DEPLOYMENT_RELEASE_ID, deploymentTriggerReason.getReleaseIdString());
            result.put(DEPLOYMENT_RELEASE_NAME, deploymentTriggerReason.getReleaseName());
        }
        return result;
    }

    public void setCachedPlanManager(CachedPlanManager cachedPlanManager)
    {
        this.cachedPlanManager = cachedPlanManager;
    }

    public void setChangeDetectionManager(ChangeDetectionManager changeDetectionManager)
    {
        this.changeDetectionManager = changeDetectionManager;
    }
}
