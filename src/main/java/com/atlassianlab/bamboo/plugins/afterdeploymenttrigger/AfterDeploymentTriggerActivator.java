package com.atlassianlab.bamboo.plugins.afterdeploymenttrigger;

import com.atlassian.bamboo.trigger.TriggerActivator;
import com.atlassian.bamboo.trigger.TriggerDefinition;
import com.atlassian.bamboo.trigger.Triggerable;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Date;

public class AfterDeploymentTriggerActivator implements TriggerActivator
{
    private static final Logger log = Logger.getLogger(AfterDeploymentTriggerActivator.class);

    @Override
    public void initAndActivate(@NotNull Triggerable triggerable, @NotNull TriggerDefinition triggerDefinition, @Nullable Date lastShutdownTime)
    {
        log.debug("init and activate");
    }

    @Override
    public void activate(@NotNull Triggerable triggerable, @NotNull TriggerDefinition triggerDefinition)
    {
        log.debug("activate");
    }

    @Override
    public void deactivate(@NotNull Triggerable triggerable, @NotNull TriggerDefinition triggerDefinition)
    {
        log.debug("deactivate");
    }
}
