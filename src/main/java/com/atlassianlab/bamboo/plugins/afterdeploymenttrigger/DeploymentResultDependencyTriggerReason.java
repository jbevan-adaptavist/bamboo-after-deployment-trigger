package com.atlassianlab.bamboo.plugins.afterdeploymenttrigger;

import com.atlassian.bamboo.core.BambooCustomDataAware;
import com.atlassian.bamboo.resultsummary.ResultsSummary;
import com.atlassian.bamboo.v2.build.trigger.AbstractTriggerReason;
import com.atlassian.bamboo.v2.build.trigger.TriggerReason;
import com.atlassian.sal.api.message.I18nResolver;
import org.jetbrains.annotations.NotNull;

import java.util.Map;

public class DeploymentResultDependencyTriggerReason extends AbstractTriggerReason implements TriggerReason
{
    static final String PLUGIN_KEY = "com.atlassianlab.bamboo.plugins.bamboo-after-deployment-trigger-plugin:deploymentResultDependencyTriggerReason";
    static final String DEPLOYMENT_PROJECT_ID_KEY = "deploymentResultDependency.triggeringDeploymentProjectId";
    static final String DEPLOYMENT_PROJECT_NAME_KEY = "deploymentResultDependency.triggeringDeploymentProjectName";
    static final String ENVIRONMENT_ID_KEY = "deploymentResultDependency.triggeringEnvironmentId";
    static final String ENVIRONMENT_NAME_KEY = "deploymentResultDependency.triggeringEnvironmentName";
    static final String RESULT_ID_KEY = "deploymentResultDependency.triggeringResultId";
    static final String RELEASE_ID_KEY = "deploymentResultDependency.triggeringReleaseId";
    static final String RELEASE_NAME_KEY = "deploymentResultDependency.triggeringReleaseName";

    private transient I18nResolver i18nResolver;

    private String deploymentProjectIdString;
    private String deploymentProjectName;
    private String environmentIdString;
    private String environmentName;
    private String resultIdString;
    private String releaseIdString;
    private String releaseName;

    @Override
    public void init(final String key, final Map<String, String> fields)
    {
        init(key);
        this.deploymentProjectIdString = fields.get(DEPLOYMENT_PROJECT_ID_KEY);
        this.deploymentProjectName = fields.get(DEPLOYMENT_PROJECT_NAME_KEY);
        this.environmentIdString = fields.get(ENVIRONMENT_ID_KEY);
        this.environmentName = fields.get(ENVIRONMENT_NAME_KEY);
        this.resultIdString = fields.get(RESULT_ID_KEY);
        this.releaseIdString = fields.get(RELEASE_ID_KEY);
        this.releaseName = fields.get(RELEASE_NAME_KEY);
    }

    @Override
    public void init(final String key, @NotNull final ResultsSummary resultsSummary)
    {
        init(key, resultsSummary.getCustomBuildData());
    }

    @Override
    public void updateCustomData(@NotNull final BambooCustomDataAware customDataAware)
    {
        updateIfValueNotNull(customDataAware, ENVIRONMENT_NAME_KEY, environmentName);
        updateIfValueNotNull(customDataAware, ENVIRONMENT_ID_KEY, environmentIdString);
        updateIfValueNotNull(customDataAware, DEPLOYMENT_PROJECT_ID_KEY, deploymentProjectIdString);
        updateIfValueNotNull(customDataAware, DEPLOYMENT_PROJECT_NAME_KEY, deploymentProjectName);
        updateIfValueNotNull(customDataAware, RESULT_ID_KEY, resultIdString);
        updateIfValueNotNull(customDataAware, RELEASE_ID_KEY, releaseIdString);
        updateIfValueNotNull(customDataAware, RELEASE_NAME_KEY, releaseName);
    }

    @Override
    public String getName()
    {
        return "Deployment result dependency";
    }

    @Override
    public String getNameForSentence()
    {
        return i18nResolver.getText("build.triggerreason.deployment.result.dependency", environmentName);
    }

    public String getDeploymentProjectIdString()
    {
        return deploymentProjectIdString;
    }

    public String getDeploymentProjectName()
    {
        return deploymentProjectName;
    }

    public String getEnvironmentIdString()
    {
        return environmentIdString;
    }

    public String getEnvironmentName()
    {
        return environmentName;
    }

    public String getResultIdString()
    {
        return resultIdString;
    }

    public String getReleaseIdString()
    {
        return releaseIdString;
    }

    public String getReleaseName()
    {
        return releaseName;
    }

    public void setI18nResolver(I18nResolver i18nResolver)
    {
        this.i18nResolver = i18nResolver;
    }
}
